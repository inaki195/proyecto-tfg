package com.ifb.inaki.util;

import com.ifb.inaki.mapeo.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.spi.ServiceException;

public class HibernateUtil {

  private static SessionFactory sessionFactory;
  private static Session session;
	
  /**
   * Crea la factoria de sesiones
   */
  public static void buildSessionFactory() {

    Configuration configuration = new Configuration();
    configuration.configure();
    // Se registran las clases que hay que mapear con cada tabla de la base de datos
    configuration.addAnnotatedClass(Citas.class);
    configuration.addAnnotatedClass(Clientes.class);
    configuration.addAnnotatedClass(Credenciales.class);
    configuration.addAnnotatedClass(Fisios.class);
    configuration.addAnnotatedClass(Incidencias.class);
    configuration.addAnnotatedClass(Usuarios.class);

    try {
      ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
              configuration.getProperties()).build();
      sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }catch (ServiceException e1){
      Util.mensajeError("no se pudo conectar con el servidor");
    }
  }
	
  /**
   * Abre una nueva sesión
   */
  public static void openSession() {
    session = sessionFactory.openSession();
  }

  /**
   * te dice si hay conexion o no
   * @return boolean
   */
  public static boolean ssesionFactory(){
   return sessionFactory.isOpen();
  }
  /**
   * Devuelve la sesión actual
   * @return session activa
   */
  public static Session getCurrentSession() {
	
    if ((session == null) || (!session.isOpen()))
      openSession();
			
    return session;
  }
  public  static void nativeSQL(String sql){
 try {
//   Session session=HibernateUtil.getCurrentSession().createNativeQuery("select  * from incidencias,clientes,")
 }catch (Exception e2){}
    
  }
	
  /**
   * Cierra Hibernate
   */
  public static void closeSessionFactory() {
	
    if (session != null)
      session.close();
		
    if (sessionFactory != null)
      sessionFactory.close();
  }
}