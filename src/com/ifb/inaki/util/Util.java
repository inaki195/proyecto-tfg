package com.ifb.inaki.util;


import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.ifb.inaki.mvc.Controlador;
import com.ifb.inaki.mvc.Login;
import com.ifb.inaki.mvc.Modelo;

import javax.swing.*;
import java.text.NumberFormat;

/**
 * clase que contiene varios metodos utilizados en cualquier clase del codigo
 */
public class Util {
    /**
     * muestra el mensaje de error con el mensaje indicado
     * @param mensaje String a mostrar
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * muestra un mensaje emergente
     * @param mensaje String  frase a mostrar
     */
    public static void mensajeWarning(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"advertencia", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * muestra un mensaje emergente
     * @param mensaje String a mostrar
     */
    public static void mensajeWarningEmpty(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"campos vacios", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * muestra una ventana emergente
     * @param msg String  del mensaje
     * @param tema String titulo de la  ventana
     */
    public static void msgConfirmacion(String msg,String tema){
        JOptionPane.showMessageDialog(null, msg,tema, JOptionPane.WARNING_MESSAGE);
    }




    /**
     * metodo que manda un mensaje dependiendo del valo booleano
     * @param credencial {@link Boolean}
     * @return Boolean
     */
    public static Boolean comprobarCuenta(Boolean credencial) {
        if (credencial){
            JOptionPane.showMessageDialog(null, "credenciales correctas","acceso permitido", JOptionPane.WARNING_MESSAGE);
            return true;
        }else {
            JOptionPane.showMessageDialog(null, "credenciales incorrectas","acceso denagado", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    /**
     * metodo incial que ejecuta el programa
     */
    public static void desplegarAplcacion() {
        Modelo modelo=new Modelo();
        Login login=new Login();
        Controlador controlador;
        controlador=new Controlador(modelo,login);
    }

    /**
     * entablece una conexion con la base de datos
     * @return conexion a la base de datos
     */
    public static Connection conectar() {
        Connection con=null;
        String url="jdbc:mysql://127.0.0.1:3306/gestionfisio?user=root&password=";
        try {
            con= DriverManager.getConnection(url);

        } catch (SQLException e) {
            System.out.println("no se pudo establecer conexion con la base de datos");
        }
        return con;
    }

}
