package com.ifb.inaki.util;

import com.ifb.inaki.mapeo.Clientes;
import com.ifb.inaki.mapeo.Credenciales;
import com.ifb.inaki.mapeo.Fisios;
import com.ifb.inaki.mapeo.Usuarios;

/**
 * clase que guarda informacion de objetos acerca del
 * usuario que ha iniciado
 */
public class Sesion {
    private Fisios fisios;
    private Clientes clientes;
    private Usuarios usuario;
    private Credenciales credenciales;

    public Credenciales getCredenciales() {
        return credenciales;
    }

    public void setCredenciales(Credenciales credenciales) {
        this.credenciales = credenciales;
    }

    public Sesion() {
    }

    public Fisios getFisios() {
        return fisios;
    }

    public void setFisios(Fisios fisios) {
        this.fisios = fisios;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }




}
