package com.ifb.inaki.mvc;

import java.awt.GridBagConstraints;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author inaki
 * JDialog  que crea un dialogo para mostrar la ventana
 * 	para crear una cuenta
 */
public class Campo extends JDialog {
	 JPanel pn_campfisio;
	 JButton btn_fisio_crearac;
     JButton btn_fisio_cancel;
     JButton btn_empty;
     JTextField txt_estudios;
     JComboBox cb_fisio;
     JLabel lbl_estudios;
     JLabel lbl_rango;
     JLabel lbl_empty;
     JLabel lbl_empty2;
     GridBagConstraints ggc;
	public Campo() {
		ggc=new GridBagConstraints();
		pn_campfisio=new JPanel();
		inicializador();
		setContentPane(pn_campfisio);
		  setSize(300,200);
		  setResizable(false);
		
		setTitle("datos fisioterapeuta");
		 ArrayList<String>lisrango=new ArrayList<>();
	        lisrango.add("admin");
	        lisrango.add("trabajador");
	        rellenarcombo(lisrango);
	        posicionamiento();
	        setVisible(true);
	}
	/**
	 * organiza la posicion del {@link GridBagConstraints}
	 * @param x eje horizontal
	 * @param y eje vertical
	 */
	private void organizador(int x, int y) {
		ggc.gridx=x;
		ggc.gridy=y;
	}

	/**
	 * metodo que posiciona  los componentes Jsiwng
	 */
	 private void posicionamiento() {
		organizador(0, 0);
		pn_campfisio.add(lbl_estudios,ggc);
		organizador(1, 0);
		pn_campfisio.add(txt_estudios,ggc);
		organizador(2, 0);
		pn_campfisio.add(lbl_empty,ggc);
		organizador(0, 1);
		pn_campfisio.add(lbl_rango,ggc);
		organizador(1, 1);
		ggc.gridy=1;pn_campfisio.add(cb_fisio,ggc);
		organizador(1, 2);
		pn_campfisio.add(btn_fisio_crearac);
		organizador(2, 1);
		pn_campfisio.add(lbl_empty2,ggc);
		
		
		
	}

	/**
	 * inicializar componentes graficos
	 */
	private void inicializador() {
		btn_fisio_cancel=new JButton();
		btn_fisio_crearac=new JButton("crear");
		btn_fisio_crearac.setActionCommand("crearfisio");
		txt_estudios=new JTextField(15);
		cb_fisio=new JComboBox<>();
		lbl_estudios=new JLabel("estudios ");
		lbl_rango=new JLabel("    rango   ");
		lbl_empty=new JLabel("  ");
		lbl_empty2=new JLabel("    ");
		btn_empty=new JButton("");
		
	}
	/**
     * recorre un array para rellenar el {@link javafx.scene.control.ComboBox}
     * @param lisrango {@link ArrayList} String
     */
    private void rellenarcombo(ArrayList<String> lisrango) {
        for (String campo:lisrango){
            cb_fisio.addItem(campo);
        }

    }
}
