package com.ifb.inaki.mvc;

import com.github.lgooddatepicker.components.DatePicker;
import com.ifb.inaki.mapeo.Citas;
import com.ifb.inaki.mapeo.Clientes;
import com.ifb.inaki.mapeo.Fisios;
import com.ifb.inaki.mapeo.Incidencias;

import javax.swing.*;
import java.awt.event.WindowEvent;

public class Vistafisio extends JFrame {
     JMenu cuenta;
    JMenuBar menu;
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
     JList listIncidencias;
     JTextField txt_fisio_nombre;
     JTextField txt_fisio_apellido;
     JTextField txt_fisio_correo;
     JTextField txt_fisio_estudios;
     JButton btn_fisio_change_modificar;
     JButton btn_fisio_change_passwd;
     JList listFisios;
     JButton btn_delete_fisios;
    JButton btn_fisio_change_deshacer;
     JButton btn_fisio_change_delete;
     JButton btn_fisio_pass;
     JButton btn_fisio_denay;
     JTextField txt_cita_precio;
     JTextField txt_fisio_cita_lugar;
     JTextField txt_cita_hora;
     JList listaincidenciascita;
     JButton btn_cita_add;
     JButton btn_cita_modifcar;
     JButton btn_cita_delete;
     JList listacitas;
     JComboBox cb_cita_cliente;
     JTextField txt_cita_minutes;
     DatePicker fechacita;
     JList listaclients;
     JButton btn_admin_cliente;
     JButton btn_fisio_informe;
    JTextField txt_fisio_nick;
    DefaultListModel<Incidencias>dlmIncidencias;
    DefaultListModel<Incidencias>dlmIncidenciascita;
    DefaultListModel<Citas>dlmcitas;
    DefaultListModel<Fisios> dlmlistafisio;
    DefaultListModel<Clientes>dlmlistacliente;
    JMenuItem cerrar;
    public Vistafisio() {
        menu=new JMenuBar();
        cuenta=new JMenu("cuenta");
        menu.add(cuenta);
        cerrar=new JMenuItem("cerrar sesión");

        cuenta.add(cerrar);
        cerrar.setActionCommand("closefisio");
        setJMenuBar(menu);
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        initmdl();
    }

    private void initmdl() {
        dlmlistacliente=new DefaultListModel<>();
        dlmlistafisio =new DefaultListModel<>();
        dlmcitas=new DefaultListModel<>();
        dlmIncidencias=new DefaultListModel<>();
        dlmIncidenciascita=new DefaultListModel<>();
        listIncidencias.setModel(dlmIncidencias);
        listaincidenciascita.setModel(dlmIncidenciascita);
        listacitas.setModel(dlmcitas);
        listFisios.setModel(dlmlistafisio);
        listaclients.setModel(dlmlistacliente);
    }


    public void cerrarVentanaFisio() {
        this.dispatchEvent(new WindowEvent(this,WindowEvent.WINDOW_CLOSING));
    }
}
