package com.ifb.inaki.mvc;

import com.ifb.inaki.mapeo.Incidencias;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Clase que representa el JFrame del cliente
 */
public class Vistacliente extends JFrame{
    JMenuBar menu;
    JMenu cuenta;
    JMenuItem cerrar;
    private JPanel panel1;
    private JLabel lbl_cliente_hello;
     JLabel lbl_empty;
     JButton btn_account_profile;
     JTextField txt_cliente_molestia;
     JComboBox cb_cliente_nivel_dolor;
     JCheckBox cbx_cliente_reincidente;
     JList list_cliente_incidencias;
     JTextField txt_account_busqueda;
     JButton btn_add_dolencia;
     JButton btn_modificar_dolencia;
     JButton btn_delete_dolencia;
     JLabel lbl_user;
     JButton btn_citas_clientes;
    DefaultListModel<Incidencias>dlmIncidencias;

    /**
     * constructor de la clase
     */
    public Vistacliente() {
        menu=new JMenuBar();
        cuenta=new JMenu("cuenta");
        menu.add(cuenta);
         cerrar=new JMenuItem("cerrar sesión");
        cuenta.add(cerrar);
        cerrar.setActionCommand("closecliente");
        setJMenuBar(menu);
        lbl_user=new JLabel("usuarió");
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       setVisible(true);
       rellenarCombo();
       initList();
       lbl_user.setText("cotel");
        //pack();
        setSize(600,600);
    }

    /**
     * metodo que inicializa un defaultlistmodel
     * para mas tarde add a un JList
     */
    private void initList() {
        dlmIncidencias=new DefaultListModel<>();
        list_cliente_incidencias.setModel(dlmIncidencias);
    }

    /**
     * rellenar el combobox
     */
    private void rellenarCombo() {
        ArrayList<String>array=new ArrayList<>();
        array.add("leve");
        array.add("moderado");
        array.add("grave");
        for (String word:array) {
            cb_cliente_nivel_dolor.addItem(word);
        }
    }



}
