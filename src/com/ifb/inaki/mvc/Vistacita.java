package com.ifb.inaki.mvc;

import com.ifb.inaki.mapeo.Citas;

import javax.swing.*;

/**
 * clase Jdialog que crea una ventana
 */
public class Vistacita extends JDialog {
    private JPanel contentPane;
     JList listacitas;
     JButton btn_cliente_cita;
    private JButton buttonOK;
    DefaultListModel<Citas>dlmcitas;

    /**
     * constructor de la clase
     */
    public Vistacita() {
        setContentPane(contentPane);
        setSize(300,400);
        setVisible(true);
        initdlm();
    }

    /***
     * inicializa  el defaultlistModel
     */
    private void initdlm() {
        dlmcitas=new DefaultListModel<>();
        listacitas.setModel(dlmcitas);
    }
}
