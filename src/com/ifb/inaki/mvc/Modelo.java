package com.ifb.inaki.mvc;

import com.ifb.inaki.mapeo.*;
import com.ifb.inaki.util.Sesion;
import com.ifb.inaki.util.HibernateUtil;
import com.ifb.inaki.util.Util;
import encriptado.Encriptacion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.hibernate.*;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import javax.persistence.PersistenceException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Modelo {
    Encriptacion encriptacion;
    private static Session session;
    SessionFactory sessionFactory;
    private ArrayList<Incidencias>dolencias;
    private ArrayList<Fisios>fisios;
    private ArrayList<Clientes>clientes;
    ArrayList<Incidencias>incidenciaspendientes;
    ArrayList<Incidencias> incidenciasDisponibles;
    ArrayList<Citas>citas;
    ArrayList<Fisios> listfisios;
    ArrayList<Clientes>listclientes;

    public Modelo() {
        citas=new ArrayList<>();
        incidenciasDisponibles =new ArrayList<>();
        incidenciaspendientes=new ArrayList<>();
        dolencias=new ArrayList<>();
        fisios=new ArrayList<>();
        clientes=new ArrayList<>();
        listfisios =new ArrayList<>();
        listclientes=new ArrayList<>();
    }

    public void rellenar() {
        rellenarIncidenciasGenerales();
    }

    /**
     * metodo que hace una consulta hql y obtiene todas las incidencias
     * para posteriormente cambia el valor de un Array
     */
    public void rellenarIncidenciasGenerales() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Incidencias ");
        ArrayList<Incidencias> incidencias = (ArrayList<Incidencias>) query.list();
        setIncidenciaspendientes(incidencias);
    }
    /**
     * @param  credencial  objeto {@link Credenciales}
     * metodo que hace una consulta hql y obtiene todas las credenciales
     * para comprobar que se han metido bien las credenciales
     * @return  Boolean indicando si la credenciales coinciden
     */
    public static Boolean comprobarCredenciales(Credenciales credencial) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Credenciales ");
        ArrayList<Credenciales> credenciales = (ArrayList<Credenciales>) query.list();
        for (Credenciales cre:credenciales) {
            if ((cre.getUser().equalsIgnoreCase(credencial.getUser()))
                    &&(cre.getPasswd().equalsIgnoreCase(credencial.getPasswd()))){
                return true;
            }
        }
        return false;
    }


    /**
     * conecta con hibernate
     */
    public void conectar() {
        HibernateUtil.buildSessionFactory();
        encriptacion=new Encriptacion();

    }

    /**
     * cierra la cesion de hibernate
     */
    public void desconectar(){
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
        HibernateUtil.closeSessionFactory();
    }

    /**
     * crea un usuario Fisio
     * @param users objeto usuario
     * @param fisios objeto fisio
     * @param credenciales objeto clie
     */
    public void crearUsuarioFisio(Usuarios users, Fisios fisios, Credenciales credenciales) {
        introducirUser(users);
        fisios.setUsuario(comprobarFisio(users.getDni()));
        introducirFisio(fisios);
        credenciales.setUsuario(comprobarCredenciales(credenciales.getUser(), credenciales.getPasswd()));
        introducirCredencial(credenciales);
    }

    /**
     * crea un usuario Cliente
     * @param user objeto usuario
     * @param clientes objeto  cliente
     * @param credenciales objeto credencial
     */
    public void crearUsuarioCliente(Usuarios user, Clientes clientes, Credenciales credenciales) {
        introducirUser(user);
        clientes.setUsuario(comprobarFisio(user.getDni()));
        introducirCliente(clientes);
        credenciales.setUsuario(comprobarCredenciales(credenciales.getUser(), credenciales.getPasswd()));
        introducirCredencial(credenciales);
    }


    /**
     * metodo que hace una query devolviendote todos los usuarios
     * @param user String
     * @param passwd String
     * @return Usuario al que hace referencia
     */
    private Usuarios comprobarCredenciales(String user, String passwd) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuarios ");
        ArrayList<Usuarios> usuarios = (ArrayList<Usuarios>) query.list();
        for (Usuarios usero:usuarios) {
            if ((usero.getPasswd().equalsIgnoreCase(passwd)) && (usero.getNickname().equalsIgnoreCase(user))){

                return usero;
            }
        }
        Usuarios su=new Usuarios();
        return su;
    }

    /**
     * devuelve  un objeto usuario que coincida con el dni
     * se obtienen todos los usuarios y se escoge el que tenga el mismo dni
     * @param dni {@link String} dni del usuario
     * @return Usuario coincidente con el dni
     */
    public Usuarios comprobarFisio(String dni) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuarios ");
        ArrayList<Usuarios> usuarios = (ArrayList<Usuarios>) query.list();
        for (Usuarios user:usuarios) {
            if (user.getDni().equalsIgnoreCase(dni)){
                return user;
            }else{
                return null;
            }
        }
         Usuarios su=new Usuarios();
        return su;
    }
    private void permisoModiciacion() {
        Query sqlQuery = HibernateUtil.getCurrentSession().
                createSQLQuery("SET SQL_SAFE_UPDATES=0;");
        sqlQuery.executeUpdate();
    }

    /**
     * introduce una credencia a la base de datos
     * @param credenciales objeto a insertar en la base de datos
     */
    public void introducirCredencial(Credenciales credenciales) {
        try {
            session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.saveOrUpdate(credenciales);
            session.getTransaction().commit();
        }catch (IllegalStateException e2){

        }
    }

    /**
     * introduce una Incidencia a la base de datos
     * @param incidencias {@link Incidencias}
     */
    public void introducirDolencia(Incidencias incidencias) {
        Session session=null;
         session=HibernateUtil.getCurrentSession();
        try {
            session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.saveOrUpdate(incidencias);
            session.getTransaction().commit();
            session.close();
        }catch (IllegalStateException e3){}

    }

    /**
     * introduce un objeto Fisio en la base de datos
     * @param fisios objeto a meter en la base de datos
     */
    public void introducirFisio(Fisios fisios) {
        session=HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(fisios);
        session.getTransaction().commit();

    }

    /**
     * introduce un objeto cliente a la base de datos
     * @param clientes objeto que se insertara en base de datos
     */
    public void introducirCliente(Clientes clientes) {
        try {
            session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.saveOrUpdate(clientes);
            session.getTransaction().commit();
        }catch (NonUniqueObjectException e3){

        }
    }

    /**
     * introduce un objeto Cita en la base de datos
     * @param citas {@link Citas} cita add
     */
    public void introducirCita(Citas citas) {
        try {
            session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.saveOrUpdate(citas);
            session.getTransaction().commit();
        }catch (NonUniqueObjectException e3){

        }
    }

    /**
     * introduce un usuario en la base de datos
     * @param users usuario a insertar en la base de datos
     */
    public void introducirUser(Usuarios users) {
        session=HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(users);
        session.getTransaction().commit();
    }

    /**
     * obtenemos todas las credencianles y filtramos para obtener un objeto Usuario
     * @return Usuario
     * @param init objeto Credencial
     */
    public Usuarios obtenerReferenciaUser(Credenciales init) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Credenciales ");
        ArrayList<Credenciales> credenciales = (ArrayList<Credenciales>) query.list();
        for (Credenciales cre:credenciales) {
            if((cre.getUser().equalsIgnoreCase(init.getUser()))
                    &&(cre.getPasswd().equalsIgnoreCase(init.getPasswd()))){
                return cre.getUsuario();

            }

        }
        return  null;
    }
    /**
     * obtienes todas las citas de la base de datos haciendo una consulta
     * @return ArrayList  de todas Citas
     */
    public ArrayList<Citas> obtenerCitas(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Citas ");
        ArrayList<Citas> listcitas = (ArrayList<Citas>) query.list();
        return listcitas;
    }

    /**
     * obtienes todos los fisios de la base de datos haciendo una consulta
     * @return ArrayList  de todos Fisioterapeutas
     */
    public ArrayList<Fisios> obtenerfisioList() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM  Fisios ");
        ArrayList<Fisios> listfisios = (ArrayList<Fisios>) query.list();
        return listfisios;
    }

    /**
     * obtienes todos los Usuarios de la base de datos haciendo una consulta
     * @return Array de usuarios
     */
    public ArrayList<Usuarios> obtenerUsuarios() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM  Usuarios ");
        ArrayList<Usuarios> listfisios = (ArrayList<Usuarios>) query.list();
        return listfisios;
    }
    /**
     * obtienes todos las Credenciales de la base de datos haciendo una consulta
     * @return ArrayList  de todos las Credenciales
     */
    public ArrayList<Credenciales> obtenerCredencialeslist() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM  Credenciales ");
        ArrayList<Credenciales> listIncidencias = (ArrayList<Credenciales>) query.list();
        return listIncidencias;
    }

    /**
     * metodo que devuelve un objeto {@link Clientes} pasandole credenciales
     * @param init credencial
     * @return Cliente al que hace referencia
     */
    public Clientes obtenerReferenciaCliente(Credenciales init) {
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Clientes c WHERE c.nickname = :nombre");
        query.setParameter("nombre", init.getUser());
        Clientes clientes = (Clientes) query.uniqueResult();
       // System.out.println(cliente.getId()+""+cliente.getNombre()+""+cliente.getApellidos());

        return clientes;
    }

    /**
     * comprueba que el nickname no lo tenga otro usuario y que no se puedan resgistrar con un mismo
     * nickname
     * @param user {@link Usuarios} a comprobar
     * @return Boolean indica si usuario esta en la base de datos
     */
    public Boolean uniqueNickname(Usuarios user) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuarios ");
        ArrayList<Usuarios> nickArray = (ArrayList<Usuarios>) query.list();
        for (Usuarios user1:nickArray) {
                if (user1.getNickname().equalsIgnoreCase(user.getNickname())){
                    System.out.println("nick repetido");
                    return true;
                }
            }
        return false;
        }

    /**
     * comprueba que la password coincida con la de la base de datos
      * @param passwd password
     * @param user nick del usuario
     * @return Boolean indica si la password es la correcta
     */
    public Boolean comprobarOldPasswd(String passwd, String user) {
        Credenciales credenciales =obtenerCredencial(user);
        String contrasena=encriptacion.desencriptacion("secret", credenciales.getPasswd());
       // System.out.println("esta es su passwd "+contrasena);
        if (!contrasena.equalsIgnoreCase(passwd)){
            Util.mensajeError("la contraseña antigua  no es la correspondiente");
            return false;
        }else{
            Util.mensajeWarning("la contraseña se puede cambiar");
            return true;
        }
    }

    /**
     * modifica un cliente
     * @param c1 {@link Clientes} cliente a modificar
     */
    public void modificarClienteObjeto(Clientes c1) {
        try {
            Session sesion = HibernateUtil.getCurrentSession();
            sesion.beginTransaction();
            sesion.save(c1);
            sesion.getTransaction().commit();
            sesion.close();
        }catch (PersistenceException e1) {

        }
    }

    /**
     * modifica datos del cliente
     * @param cesion contiene credenciales ,cliente,Usuario
     * @param name nombre
     * @param apellido apellido
     * @param nickText nickname
     * @param passwd contrasena
     */
    public void ModificarCliente(Sesion cesion, String name, String apellido, String nickText, String passwd) {
        String nickname=cesion.getUsuario().getNickname();
        Usuarios modificaruser=obtnerusuario(nickname);
        modificaruser.setNombre(name);
        modificaruser.setApellidos(apellido);
        modificaruser.setNickname(nickText);
        modificaruser.setPasswd(encriptacion.encriptado("secret",passwd));
        Clientes modificarClientes =obtenerCliente(nickname);
        modificarClientes.setNombre(name);
        modificarClientes.setApellidos(apellido);
        modificarClientes.setNickname(nickText);
        modificarClientes.setPasswd(encriptacion.encriptado("secret",passwd));
        Credenciales credenciales =obtenerCredencial(nickname);
        credenciales.setUser(nickText);
        credenciales.setPasswd(encriptacion.encriptado("secret",passwd));
      //  System.out.println("crendencial "+credencial.getId()+" clientes "+modificarCliente.getId()+" usuario"+modificaruser.getId());
        modificarinfo(modificarClientes,modificaruser, credenciales);
    }

    private void modificarinfo(Clientes modificarClientes, Usuarios modificaruser, Credenciales credenciales) {
        try {
            modificarUserhb(modificaruser);
            modificarCredenciales(credenciales);
            modificarClientehb(modificarClientes);
        }catch (ConstraintViolationException e3){
            System.out.println(" ");
        }
    }

    /**
     * modificar un fisio de la base de datos
     * @param fisios a modificar
     */
    public void modificarfisioterapeuta(Fisios fisios) {
        try {
            Session sesion = HibernateUtil.getCurrentSession();
            sesion.beginTransaction();
            sesion.saveOrUpdate(fisios);
            sesion.getTransaction().commit();
            sesion.close();
        }catch (IllegalStateException e2){

        }

    }

    /**
     * modificar una objeto cliente a traves de lenguaje HQL
     * @param clientes a modificar
     * @throws ConstraintViolationException excepcion
     */
    public void modificarClientehb(Clientes clientes)throws ConstraintViolationException{

          try {
                             Query query = HibernateUtil.getCurrentSession().createSQLQuery("update Clientes u set  =:nombre , =:apellidos,=:nickname,=:passwd" +
                                   " where u.id=:id ");
                              query.setParameter("id", clientes.getUsuario().getId());
                              query.setParameter("nombre", clientes.getNombre());
                              query.setParameter("apellidos", clientes.getApellidos());
                              query.setParameter("nickname", clientes.getNickname());
                              query.setParameter("passwd", clientes.getPasswd());
                              query.executeUpdate();
                          }catch (TransactionException e1){
                                   System.out.println(" ");
                          }catch (PersistenceException e3){

          }
    }

    /**
     * modifica una credencial de la base de datos
     * @param credenciales a modificar
     * @throws ConstraintViolationException excepcion
     */
    public void modificarCredenciales(Credenciales credenciales)throws ConstraintViolationException {

        Session sesion = HibernateUtil.getCurrentSession();
        System.out.println(sesion.isOpen());
        sesion.beginTransaction();
        sesion.save(credenciales);
        sesion.getTransaction().commit();
        sesion.close();
        System.out.println(sesion.isOpen());
    }

    /**
     * modifica una incidencia de la base de datos
     * @param incidencias a modificar
     */
    public void modificarIncidencia(Incidencias incidencias) {
        try {
            Session session=null;
             session = HibernateUtil.getCurrentSession();
            session.beginTransaction();
            session.persist(incidencias);
            session.getTransaction().commit();
            session.close();

        }catch (PersistenceException e1) {

        }catch (IllegalStateException e3){

        }

    }

    /**
     * modificar una cita en la base de datos
     * @param citas a mooficar
     */
    public void modificarCita(Citas citas) {
        try {
            Session sesion = HibernateUtil.getCurrentSession();
            sesion.beginTransaction();
            sesion.save(citas);
            sesion.getTransaction().commit();
            sesion.close();
        }catch (ConstraintViolationException e3){

        }
    }

    /**
     * modifica  un usuario en la base de datos
     * @param modificaruser usuario  a modificar
     */
    public void modificarUserhb(Usuarios modificaruser) {
        try {
            Session sesion = HibernateUtil.getCurrentSession();
            sesion.beginTransaction();
            sesion.saveOrUpdate(modificaruser);
            sesion.getTransaction().commit();
            sesion.close();
        }catch (ConstraintViolationException e3){

        }
    }

    /**
     * obtiene el cliente que coincide con nick que recibe como parametro
     * @param nickname nick  con el que te registras
     * @return Cliente al que hace referencia
     */
    public Clientes obtenerCliente(String nickname) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Clientes c where c.nickname=:usero");
        query.setParameter("usero",nickname);
        Clientes clientes = (Clientes) query.uniqueResult();
        return clientes;
    }

    /**
     * obtiene la credencial de la base de datos
     * @param usero nombre de usuario
     * @return Credenciales del usuario correspondiente
     */
    public Credenciales obtenerCredencial(String usero) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Credenciales c where c.user=:usero");
        query.setParameter("usero",usero);
        Credenciales credenciales = (Credenciales) query.uniqueResult();
        return credenciales;
    }

    /**
     * obtiene un usuario con el nick que mandas mendiante una consulta
     * @param nick nombre de usuario
     * @return objeto {@link Usuarios}
     */
    public Usuarios obtnerusuario(String nick) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuarios c where c.nickname=:user");
        query.setParameter("user",nick);
        Usuarios usuario = (Usuarios) query.uniqueResult();
        return usuario;
    }


    /***
     * borra un cliente y todos sus datos asociados
     * @param clientedelete cliente a eliminar
     * @param usuario usuario a eliminar
     * @param credenciales credencia  a eliminar
     */
    public void deleteUser(Clientes clientedelete, Usuarios usuario, Credenciales credenciales) {
        borrarRegistro(clientedelete, usuario, credenciales);
    }

    /**
     * borra un cliente entero junto con sus datos asociados
     * @param clientedelete cliente a eliminar
     * @param usuario usuario a eliminar
     * @param credenciales credencial a eliminar
     */
    private void borrarRegistro(Clientes clientedelete, Usuarios usuario, Credenciales credenciales) {
        deleteCliente(clientedelete);
        deleteCredencial(credenciales);
        deleteUserElegido(usuario);
    }

    /**
     * elimina un usuario de la base de datos
     * @param usuario elegido a eliminar
     */
    public void deleteUserElegido(Usuarios usuario) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(usuario);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * borra una crendencial de la base de datos
     * @param credenciales a eliminar
     */
    public void deleteCredencial(Credenciales credenciales) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(credenciales);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina un Cliente de la base de datos
     * @param clientedelete  cliente a eliminar
     */
    public void deleteCliente(Clientes clientedelete) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(clientedelete);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina un fisio de la base de datos
     * @param fisios a eliminar
     */
    public void deleteFisio(Fisios fisios) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(fisios);
        sesion.getTransaction().commit();
        sesion.close();
    }



    /**
     * elimina las {@link Citas}
     * @param citas a eliminar
     */
    public void deleteCita(Citas citas) {
        citas.setClientes(null);
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(citas);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * elimina una {@link Incidencias} de la base de datos
     * @param incidencias {@link Incidencias} a eliminar
     */

    public void deleteIncidencia(Incidencias incidencias) {
        Session session=null;
        incidencias.setClientes(null);
            session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.remove(incidencias);
        session.getTransaction().commit();
        session.close();


    }

    /**
     * metodo que hace una consulta que devuelve un objeto cliente
     * @param nick con el que te has  registrado
     * @return objeto {@link Clientes} al que hace referencia en nick
     */
    public Clientes addCliente(String nick) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Clientes c where c.nickname=:nick");
        query.setParameter("nick",nick);
        Clientes clientes = (Clientes) query.uniqueResult();
        return clientes;
    }
    /**
     * @param  user String nombre de usuario
     * obtienes todos los fisios de la base de datos
     * @return Array de clientes
     */
    public Fisios obtenerfisio(String user) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Fisios c where c.nickname=:nick");
        query.setParameter("nick",user);
        Fisios fisios = (Fisios) query.uniqueResult();
        return fisios;
    }

    /**
     * obtienes todos los clientes de la base de datos
     * @return ArrayList de clientes
     */
    public ArrayList<Clientes> obtenerClienteList() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Clientes");
        ArrayList<Clientes> listclientes = (ArrayList<Clientes>) query.list();
        return listclientes;
    }

    /**
     * obtiene un Array de la base de datos de las Incidencias
     * @return ArrayList de incidencias
     */
    public ArrayList<Incidencias> actualizarDolencias() {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Incidencias ");
        ArrayList<Incidencias> incidencias = (ArrayList<Incidencias>) query.list();

        return incidencias;
    }

    /**
     * recibe un array de {@link Incidencias} que tenga un nombre parecido
     * @param busqueda incidencia a buscar
     * @return ArrayList de {@link Incidencias}
     */
    public ArrayList<Incidencias> busquedaDolencia(String busqueda) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Incidencias i where i.molestia="+busqueda);
        ArrayList<Incidencias> incidencias = (ArrayList<Incidencias>) query.list();

        return incidencias;
    }




    /**
     * cambia un array por otro
     */
    public void cargarDatos() {
        setDolencias(actualizarDolencias());

    }

    public Encriptacion getEncriptacion() {
        return encriptacion;
    }

    public void setEncriptacion(Encriptacion encriptacion) {
        this.encriptacion = encriptacion;
    }

    public ArrayList<Incidencias> getDolencias() {
        return dolencias;
    }

    public void setDolencias(ArrayList<Incidencias> dolencias) {
        this.dolencias = dolencias;
    }

    public ArrayList<Fisios> getFisios() {
        return fisios;
    }

    public void setFisios(ArrayList<Fisios> fisios) {
        this.fisios = fisios;
    }

    public ArrayList<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Clientes> clientes) {
        this.clientes = clientes;
    }

    public ArrayList<Incidencias> getIncidenciaspendientes() {
        return incidenciaspendientes;
    }

    public ArrayList<Citas> getCitas() {
        return citas;
    }

    public void setCitas(ArrayList<Citas> citas) {
        this.citas = citas;
    }

    public void setIncidenciaspendientes(ArrayList<Incidencias> incidenciaspendientes) {
        this.incidenciaspendientes = incidenciaspendientes;
    }

    public ArrayList<Clientes> getListclientes() {
        return listclientes;
    }

    public void setListclientes(ArrayList<Clientes> listclientes) {
        this.listclientes = listclientes;
    }

    public ArrayList<Incidencias> getIncidenciasDisponibles() {
        return incidenciasDisponibles;
    }

    public void setIncidenciasDisponibles(ArrayList<Incidencias> incidenciasDisponibles) {
        this.incidenciasDisponibles = incidenciasDisponibles;
    }

    public ArrayList<Fisios> getListfisios() {
        return listfisios;
    }

    public void setListfisios(ArrayList<Fisios> listfisios) {
        this.listfisios = listfisios;
    }

    /**
     * modificar los datos de un fisioterapeuta
     * @param cesion objeto que contiene datos asociado
     */
    public void modificarfisio(Sesion cesion) {
        try {
            modificarfisioterapeuta(cesion.getFisios());
            modificarCredenciales(cesion.getCredenciales());
            modificarUserhb(cesion.getUsuario());
        }catch (ConstraintViolationException e4){

        }
    }

    /**
     * elimina un fisioterapeuta y sus datos asociados
     * @param cesion obtiene todos los objetos del fisio
     */
    public void deleteAccountFisio(Sesion cesion) {
      //
      deleteCredencial(cesion.getCredenciales());
      deleteFisio(cesion.getFisios());
      deleteUserElegido(cesion.getUsuario());
    }


    /**
     * comprueba si los el fisio tiene rango admin
     * @param  nickname del fisio(String)
     * @return Boolean indica si el usuario tiene rango admin
     */
    public Boolean comprobarPrivilegios(String nickname) {
        Fisios fisios =obtenerfisio(nickname);
        if (fisios.getRango().equalsIgnoreCase("admin")){
            return true;
        }else {
            return false;
        }


    }

    public void modificarIncidenciapro(Incidencias incidencias) {
        String sql = "update from ";
        Query query = session.createSQLQuery(sql);
    }

    /**
     * filtra la dolencia de cliente
     * @param clientes seleccionado
     * @return ArrayList de las {@link Incidencias} del cliente
     */
    public ArrayList<Incidencias> filtrarDolencia(Clientes clientes) {
        ArrayList<Incidencias>dolenciaspropias=new ArrayList<>();
        for (Incidencias incidencias :actualizarDolencias() ) {
            boolean elegido=false;
            for (Clientes c: incidencias.getClientes()) {
                if (c.getId()== clientes.getId()){
                    elegido=true;
                }
            }
            if (elegido){
                dolenciaspropias.add(incidencias);
            }
        }

        return dolenciaspropias;
    }

    /**
     * @param incidencias objeto {@link Incidencias}
     * add cliente  a la incidencia  asociado
     * @param incidencias a filtrar {@link Incidencias}
     * @return ArrayList de clientes
     * @throws NullPointerException no coge elementos vacios
     */
    public List<Clientes> filtraCliente(Incidencias incidencias) {

        List<Clientes> listaclientes=new ArrayList<>();
        try {
            for (Incidencias inci : actualizarDolencias()) {
                if (inci.getId() == incidencias.getId()) {
                    listaclientes = inci.getClientes();
                }
            }
         //   System.out.println("tamaño " + listaclientes.size());

        }catch (NullPointerException e2){

        }
     return listaclientes;
    }

    /**
     * coge las citas y las filtra de tal forma que devuelvan las citas del fisioterapeuta
     * @param cesion que inicio la sesion
     * @param user dice si es fisio o cliente
     * @return citas de dicho cliente o fisio
     */
    public ArrayList<Citas> filtrarCitas(Sesion cesion, int user) {
        Fisios fisios =null;
        Clientes clientes=null;
        if (user==1){
          fisios =cesion.getFisios();
        }
        if (user==2){
            clientes=cesion.getClientes();
        }
        ArrayList<Citas>filtro=new ArrayList<>();
        ArrayList<Citas>listcitas=obtenerCitas();
        for (Citas c:listcitas) {
            if (user==1){
                 if (c.getFisios().getNickname().equalsIgnoreCase(fisios.getNickname())){
                    filtro.add(c);
                }
            }
            if (user==2){
                if (c.getClientes().getNickname().equalsIgnoreCase(clientes.getNickname())){
                    filtro.add(c);
                }
            }
        }
        return filtro;
    }

    /**
     * recibes un arraylist de las incidencia que coincidida con el text
     * @param text String
     * @return ArrayList String
     */
    public ArrayList<Incidencias> filtradoBusquedasIncidencias(String text) {
        ArrayList<Incidencias>incidencias=actualizarDolencias();
        ArrayList<Incidencias>filtro=new ArrayList<>();
        for (Incidencias u:incidencias){
            if (u.getMolestia().equalsIgnoreCase(text)){
                filtro.add(u);
            }
        }
        return filtro;
    }

    public void modificaInciEstado(Incidencias incidencias) {

    }

    /**
     * metodo que genera un informe de las citas del cliente
     * @param id  del cliente
     */
    public void generarInformeCita(int id) {
        JasperPrint jasperPrint;
        try {
            JasperReport jr= (JasperReport) JRLoader.loadObject(getClass().getResource("report/intentop.jasper"));
            Connection cn=Util.conectar();
            HashMap<String,Object> parametros= new HashMap<String, Object>();
            parametros.put("id_cliente", id);
            String path="report/intentop.jasper";
            jasperPrint=JasperFillManager.fillReport(path,parametros,cn);
            JasperViewer jasperViewver=new JasperViewer(jasperPrint,false);
            jasperViewver.setVisible(true);
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



    }

    /**
     * metodo que generar informes
     */
    public void generarInformeUsuarios() {
        JasperPrint jasperPrint;
        try {
            Connection cn=Util.conectar();

            JasperReport jr= (JasperReport) JRLoader.loadObject(getClass().getResource("report/listausuarios.jasper"));
            String path="report/listausuarios.jasper";
            jasperPrint=JasperFillManager.fillReport(path,null,cn);
            JasperViewer jasperViewver=new JasperViewer(jasperPrint,false);
            jasperViewver.setVisible(true);
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
