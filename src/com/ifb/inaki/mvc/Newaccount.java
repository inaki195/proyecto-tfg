package com.ifb.inaki.mvc;



import com.github.lgooddatepicker.components.DatePicker;

import java.awt.GridBagConstraints;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

/**
 * clase que representa un componente grafico
 */
public class Newaccount extends JDialog {
	private JPanel pn_account;
	private JLabel lbl_ac_name;
	 JTextField txt_ac_nombre;
	private JLabel lbl_ac_usuario;
	private JLabel lbl_ac_apellido;
	private JLabel lbl_ac_correo;
	private JLabel lbl_ac_dni;
	private JLabel lbl_ac_nick;
	private JLabel lbl_ac_passwd;
	private JLabel lbl_ac_fecha;
	 JTextField txt_ac_apellido;
	 JTextField txt_ac_dni;
	 JTextField txt_ac_nick;
	 JTextField txt_ac_passwd;
	 JTextField txt_ac_correo;
	 JRadioButton rb_ac_fisio;
	 JRadioButton rb_ac_cliente;
	private ButtonGroup rbg_ac;
	JTextField tfLoginnick;
	JTextField tfLoginpasswd;
	DatePicker datePicker;
	JButton btn_ac_init;
	GridBagConstraints ggc;

	/**
	 * constructor de la clase
	 */
	public Newaccount() {
	
		super();

		pn_account=new JPanel();
		setContentPane(pn_account);
		ggc=new GridBagConstraints();
		iniciarlizartx();
		inicializarLB();
		inicializarRB();
		posicion();
	
	}

	/**
	 * metodo que organiza la posicion de los diferentes componentes graficos
	 */
	private void posicion() {
		organizador(0, 0);
		pn_account.add(lbl_ac_name,ggc);
		organizador(1, 0);
		pn_account.add(txt_ac_nombre,ggc);
		organizador(1, 2);
		pn_account.add(lbl_ac_apellido,ggc);
		organizador(2, 2);
		pn_account.add(txt_ac_apellido,ggc);
		organizador(1, 3);
		pn_account.add(lbl_ac_dni,ggc);
		organizador(2, 3);
		pn_account.add(txt_ac_dni,ggc);
		organizador(1, 4);
		pn_account.add(lbl_ac_nick,ggc);
		organizador(2, 4);
		pn_account.add(txt_ac_nick,ggc);
		organizador(3, 4);
		pn_account.add(lbl_ac_passwd,ggc);
		organizador(4, 4);
		pn_account.add(txt_ac_passwd,ggc);
		organizador(1, 5);
		pn_account.add(lbl_ac_correo,ggc);
		organizador(2, 5);
		pn_account.add(txt_ac_correo,ggc);
		organizador(1,7);
		pn_account.add(lbl_ac_fecha,ggc);
		organizador(2,7);
		pn_account.add(datePicker,ggc);
		organizador(0,8);
		pn_account.add(rb_ac_cliente,ggc);
		organizador(1,8);
		pn_account.add(rb_ac_fisio,ggc);
		organizador(1,9);
		pn_account.add(btn_ac_init,ggc);
		setTitle("registro de usuario");
		setSize(250,300);
		setResizable(false);
		setVisible(true);
		
	}

	/**
	 * metodo que inicializar los Radiobuttons
	 */
	private void inicializarRB() {
		rb_ac_cliente=new JRadioButton("cliente ");
		rb_ac_fisio=new JRadioButton(" fisioterapeuta");
		rbg_ac=new ButtonGroup();
		rbg_ac.add(rb_ac_cliente);
		rbg_ac.add(rb_ac_fisio);
		btn_ac_init=new JButton("aceptar");
		datePicker=new DatePicker();
	}

	/**
	 * metodo que inicializa los Jtexfield
	 */
	private void iniciarlizartx() {
		txt_ac_nombre=new JTextField(10);
		txt_ac_apellido=new JTextField(10);
		txt_ac_dni=new JTextField(10);
		txt_ac_nick=new JTextField(10);
		txt_ac_passwd=new JTextField(10);
		txt_ac_correo=new JTextField(10);
	}

	/**
	 * metodo que inicializa los atributos de las labels
	 */
	private void inicializarLB() {
		lbl_ac_name=new JLabel("   nombre  ");
		lbl_ac_apellido=new JLabel("   apellidos  ");
		lbl_ac_correo=new JLabel("  correo   ");
		lbl_ac_dni=new JLabel("numero de DNI");
		lbl_ac_nick=new JLabel("nombre de usuario");
		lbl_ac_passwd=new JLabel("   contraseña   ");
		lbl_ac_fecha=new JLabel("fecha de nacimiento");
	}

	/**
	 * organiza la posicion del {@link GridBagConstraints}
	 * @param x eje horizontal
	 * @param y eje vertical
	 */
	private void organizador(int x, int y) {
		ggc.gridx=x;
		ggc.gridy=y;
	}

    public void vaciarCampos() {
		txt_ac_passwd.setText("");
		txt_ac_correo.setText("");
		txt_ac_nick.setText("");
		txt_ac_nombre.setText("");
		txt_ac_apellido.setText("");
		txt_ac_dni.setText("");
		datePicker.setText("");
    }
}
