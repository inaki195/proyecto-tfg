package com.ifb.inaki.mvc;

import com.ifb.inaki.mapeo.*;
import com.ifb.inaki.util.Sesion;
import com.ifb.inaki.util.Util;
import encriptado.Encriptacion;
import org.hibernate.dialect.lock.OptimisticEntityLockException;
import org.hibernate.exception.ConstraintViolationException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * @author inaki
 * clase que comunica lo Visual con lo logico
 */
public class Controlador implements ActionListener, WindowListener, ListSelectionListener, KeyListener {
Modelo modelo;
Newaccount newaccount;
Login login;
Campo campo;
Encriptacion encriptacion;
Vistacliente vistacliente;
Vistafisio vistafisio;
Vistacita vistacita;
EditProfile editProfile;
Sesion cesion;

    /**
     * constructor de la clase
     * @param modelo objeto de la clase que controla los metodos logicos
     * @param login objeto de la una clase JDialog
     */
    public Controlador(Modelo modelo, Login login) {
        this.modelo = modelo;
        this.login = login;
        cesion=new Sesion();
        modelo.conectar();
        setname();
        addlistener(this);
        encriptacion=new Encriptacion();
       // System.out.println(encriptacion.desencriptacion("secret","95AnLawojlE="));


    }

    /**
     * add windowslistener a un JFrame
     */
    private void listener() {
        vistacliente.addWindowListener(this);
    }

    /**
     * cambia los nombre de los botones
     */
    private void setname() {
        login.btnLoginAccoutRegistrer.setActionCommand("resgister");
        login.btnLoginAccoutInit.setActionCommand("login");
    }


    /**
     * metodo que gestiona las acciones que pasan dependiendo
     * de que boton pulses
     * @param e {@link ActionEvent}
     */
    @Override
    public void actionPerformed(ActionEvent e) {
    String comand=e.getActionCommand();
    switch (comand){
        case "login":{

           Credenciales init=new Credenciales();
           init.setUser(login.tfLoginnick.getText());
           init.setPasswd(encriptarPasswd(login.tfLoginpasswd.getText()));
           Boolean siguiente=Util.comprobarCuenta(modelo.comprobarCredenciales(init));
           init.setUsuario(modelo.obtenerReferenciaUser(init));
           if (siguiente){
               login.cerrarlogin();
               if (init.getUsuario().getTipo().equalsIgnoreCase("cliente")){
                   vistacliente=new Vistacliente();

                    listener();
                    addlistenerClientes(this);
                    addlistenermenuCliente(this);
                    cesion.setCredenciales(init);

                    cesion.setUsuario(modelo.obtenerReferenciaUser(init));
                   cesion.setClientes(modelo.obtenerReferenciaCliente(init));

                    vistacliente.lbl_user.getText();
                    addListenerLabel(this);
                    addlistenertxt(this);
                   listarClientes();


               }
               if (init.getUsuario().getTipo().equalsIgnoreCase("fisio")){
                   cesion.setCredenciales(init);
                   cesion.setUsuario(modelo.obtenerReferenciaUser(init));
                   cesion.setFisios(modelo.obtenerfisio(init.getUser()));
                    vistafisio=new Vistafisio();
                    modelo.rellenar();
                    addlistenerfisio(this);
                   addlistnerMenuFisio(this);
                   addlistJlistlistener(this);

                   try {
                       cargardatosFisio();
                   }catch (NullPointerException e3){

                   }
                    vistafisio.txt_fisio_nombre.setText(cesion.getFisios().getNombre());
                    vistafisio.txt_fisio_apellido.setText(cesion.getFisios().getApellidos());
                    vistafisio.txt_fisio_correo.setText(cesion.getFisios().getCorreo());
                    vistafisio.txt_fisio_estudios.setText(cesion.getFisios().getEstudios());
                   addlistenerMenuFisio(this);
               }
           }else {

           }


        }
        break;
        case "savechanges":{
            String usuario=cesion.getUsuario().getNickname();
            comprobarCamposVacios();
            Boolean cambiarpasswd=modelo.comprobarOldPasswd(editProfile.txt_edit_paswd_old.getText(),usuario);
            if (cambiarpasswd) {
                String name = editProfile.txt_edit_name.getText();
                String apellido = editProfile.txt_edit_apellidos.getText();
                String nickText = editProfile.txt_edit_nick.getText();
                String passwd = editProfile.txt_edit_paswd_new.getText();
                modelo.ModificarCliente(cesion,name,apellido,nickText,passwd);
            }else {

            }
        }
        break;
        case "desahcercambiosedit":{
            editProfile.txt_edit_paswd_old.setText("");
            editProfile.txt_edit_paswd_new.setText("");
            editProfile.txt_edit_nick.setText("");
            editProfile.txt_edit_name.setText("");
            editProfile.txt_edit_apellidos.setText("");
        }
        break;
        case "deleteaccount":{
            String nick=cesion.getUsuario().getNickname();
            System.out.println(nick);
            Clientes clientedelete=modelo.obtenerCliente(nick);
            Usuarios usuario =modelo.obtnerusuario(nick);
            Credenciales credenciales =modelo.obtenerCredencial(nick);
            modelo.deleteUser(clientedelete, usuario, credenciales);
            vistacliente.setVisible(false);
            if (vistacita==null){

            }else{
                vistacita.setVisible(false);
            }
            if (editProfile==null){

            }else{
                editProfile.setVisible(false);
            }
            Util.desplegarAplcacion();
        }
        break;
        case "adddolencia":{
          boolean seguir=comprobarCamposEmptyDolencia();
          if (seguir){
              List<Clientes>listaclientes=new ArrayList<>();
              System.out.println(cesion.getUsuario().getNickname());
              Clientes clientes =modelo.addCliente(cesion.getUsuario().getNickname());
              System.out.println("add"+ clientes.getNickname());
              cesion.setClientes(clientes);
              listaclientes.add(modelo.addCliente(cesion.getUsuario().getNickname()));
              Incidencias incidencias =new Incidencias();
            //  incidencias.setId(modelo.consenguirID());
              incidencias.setEstado("pendiente");
              incidencias.setMolestia(vistacliente.txt_cliente_molestia.getText());
              incidencias.setNivelDolor((String) vistacliente.cb_cliente_nivel_dolor.getSelectedItem());
              incidencias.setReincidente(comprobarReincidencia());
              incidencias.setClientes(obtenerPancientes());
              try {
                  clientes.getDolencias().add(incidencias);
              }catch (NullPointerException e3){

              }
                  modelo.introducirDolencia(incidencias);
              vistacliente.txt_cliente_molestia.setText("");
              listarClientes();
          }else {
            Util.mensajeWarningEmpty("dejaste campos vacios");
            limpiarcamposcitas();
          }
        }
        break;
        case "deleteDolencias":{
           Incidencias incidencias = (Incidencias) vistacliente.list_cliente_incidencias.getSelectedValue();
           if (incidencias ==null){
               Util.mensajeWarning("no seleccionaste ninguna incidencia");
           }else{
               try {
                   Clientes c1=cesion.getClientes();
                   c1.getDolencias().remove(incidencias);
                  modelo.modificarClientehb(c1);
                   modelo.deleteIncidencia(incidencias);
               }catch (OptimisticEntityLockException r4){

               }
               listarClientes();
           }
        }
        break;
        case "modificardolencia":{
            try {
                String molestia = vistacliente.txt_cliente_molestia.getText();
                Incidencias incidencias = (Incidencias) vistacliente.list_cliente_incidencias.getSelectedValue();
                incidencias.setReincidente(comprobacionReincidente());
                incidencias.setNivelDolor((String) vistacliente.cb_cliente_nivel_dolor.getSelectedItem());
                incidencias.setMolestia(molestia);


                if (molestia.isEmpty()) {
                    Util.mensajeWarningEmpty("El campo de la molestia esta vacío ");
                } else {
                    Clientes c1=cesion.getClientes();
                    for (Incidencias i:c1.getDolencias()) {
                       if (i.getId()== incidencias.getId()){
                           i.setMolestia(molestia);
                           i.setReincidente(comprobacionReincidente());
                           i.setNivelDolor((String) vistacliente.cb_cliente_nivel_dolor.getSelectedItem());
                       }
                    }
                    modelo.modificarClienteObjeto(c1);
                    modelo.modificarIncidencia(incidencias);
                    listarClientes();
                }
            }catch (NullPointerException e1){
                Util.mensajeWarning("no escogiste ninguna dolencia");
            }
        }
        break;
        case "resgister":{
           newaccount=new Newaccount();
           addlistenerregister(this);
        }
        break;
        case "editarperfilcliente":{

        editProfile=new EditProfile();
        addlistenerEditProfile(this);
        Clientes clientes =modelo.obtenerCliente(cesion.getClientes().getNickname());
        editProfile.txt_edit_name.setText(clientes.getNombre());
        editProfile.txt_edit_apellidos.setText(clientes.getApellidos());
        editProfile.txt_edit_nick.setText(clientes.getNickname());
        }
        break;
        case "crearus":{

            if (newaccount.rb_ac_fisio.isSelected()){

                campo=new Campo();
                addListenercampo(this);
            }
            if(newaccount.rb_ac_cliente.isSelected()){
                Boolean repetido=false;
                Usuarios user=crearUsuario();
                Clientes clientes =crearCliente();
                Credenciales credenciales =crearCredencial();
                repetido=modelo.uniqueNickname(user);
                if (repetido){
                    Util.mensajeError("no puede registrarse con un nick que ya está registrado");
                }else {
                    modelo.crearUsuarioCliente(user, clientes, credenciales);
                    Util.msgConfirmacion("cuenta creada exitosamente","cuenta");
                    limpiarcampos(false);
                }
            }
        }
        break;
        case "crearfisio":{
            boolean repeat=false;
            Credenciales credenciales =crearCredencial();
            Usuarios users=crearUsuario();
            Fisios fisios =CrearFisio();
            repeat=modelo.uniqueNickname(users);
            if (repeat){
                Util.mensajeError("no puede registrarse con un nick que ya está registrado");
            }else{
                modelo.crearUsuarioFisio(users, fisios, credenciales);
                Util.msgConfirmacion("cuenta creada exitosamente","cuenta");
                limpiarcampos(true);
            }

        }
        break;
        case "modificarfisio":{
            if (comprobarcamposVaciosFisio()) {
                cesion.getUsuario().setNombre(vistafisio.txt_fisio_nombre.getText());
                cesion.getUsuario().setApellidos(vistafisio.txt_fisio_apellido.getText());
                cesion.getUsuario().setCorreo(vistafisio.txt_fisio_correo.getText());
                cesion.getFisios().setCorreo(vistafisio.txt_fisio_correo.getText());
                cesion.getFisios().setNombre(vistafisio.txt_fisio_nombre.getText());
                cesion.getFisios().setApellidos(vistafisio.txt_fisio_apellido.getText());
                cesion.getFisios().setEstudios(vistafisio.txt_fisio_estudios.getText());
                modelo.modificarfisio(cesion);
            }else {
                Util.mensajeWarningEmpty("te dejaste algún campo vacío");
            }

        }
        break;
        case "changepasswdfisio":{
           String oldpasswd= JOptionPane.showInputDialog("escribe tu antigua contraseña");
          Boolean cambiar= modelo.comprobarOldPasswd(oldpasswd,cesion.getUsuario().getNickname());
          if (cambiar) {
              String newpasswd = JOptionPane.showInputDialog("nueva contraseña");
              try {
                  String nick=cesion.getCredenciales().getUser();
                String  critpopasswd=encriptacion.encriptado("secret",newpasswd);
                Credenciales credenciales =modelo.obtenerCredencial(nick);
                Usuarios usuario =modelo.obtnerusuario(nick);
                Fisios fisios =modelo.obtenerfisio(nick);
                fisios.setPasswd(critpopasswd);
                usuario.setPasswd(critpopasswd);

                credenciales.setPasswd(critpopasswd);
                cesion.setCredenciales(credenciales);
                cesion.setFisios(fisios);
                cesion.setUsuario(usuario);
                modelo.modificarfisio(cesion);

              }catch (ConstraintViolationException e3){
                  System.out.println("");
              }

          }else{
             Util.mensajeWarning("no se pudo cambiar las credenciales");
          }

        }
        break;
        case"deletefisioacc":{
            String nick=cesion.getUsuario().getNickname();
            cesion.setUsuario(modelo.obtnerusuario(nick));
            cesion.setFisios(modelo.obtenerfisio(nick));
            cesion.setCredenciales(modelo.obtenerCredencial(nick));
            modelo.deleteAccountFisio(cesion);
            vistafisio.setVisible(false);
            Util.desplegarAplcacion();
        }
        break;
        case "passIncidencias":{
            boolean siguiente=comprobarPrivilegiosfisio();
            if (siguiente){
                Incidencias incidencias =null;
               incidencias =(Incidencias) vistafisio.listIncidencias.getSelectedValue();
              incidencias.setEstado("en proceso");
              modelo.modificarIncidencia(incidencias);
                refrescarFisios();
            }else {
            Util.mensajeWarning("No tienes privilegios ");
            }
        }
        break;
        case"denegarince":{
            boolean siguiente=comprobarPrivilegiosfisio();
            if (siguiente){
                Incidencias incidencias =null;
                incidencias =(Incidencias) vistafisio.listIncidencias.getSelectedValue();
                incidencias.setEstado("rechazado");
                modelo.modificarIncidencia(incidencias);
                refrescarFisios();
            }else {
                Util.mensajeWarning("No tienes privilegios ");
            }
        }
        break;
        case "addcita":{
            Citas citas =new Citas();
            Fisios fisios =cesion.getFisios();
            citas.setFisios(fisios);
            modelo.filtraCliente((Incidencias) vistafisio.listaincidenciascita.getSelectedValue());
           String cliente= (String) vistafisio.cb_cita_cliente.getSelectedItem();
           Clientes clientes=modelo.obtenerCliente(cliente);
            citas.setClientes(clientes);
            citas.setPrecio(Double.valueOf((vistafisio.txt_cita_precio.getText())));
            citas.setLugar(vistafisio.txt_fisio_cita_lugar.getText());
            int minutos= Integer.parseInt(vistafisio.txt_cita_minutes.getText());
            int horas= Integer.parseInt(vistafisio.txt_cita_hora.getText());
            Time duracion=new Time(horas,minutos,0);
            citas.setDuracion(duracion);
            citas.setFecha(Date.valueOf(vistafisio.fechacita.getDate()));
            modelo.introducirCita(citas);
            modelo.modificaInciEstado((Incidencias)vistafisio.listaincidenciascita.getSelectedValue());
            cargardatosFisio();

        }
        break;
        case"combocliente":{

        }
        break;
        case"eliminarcita":{
        Citas citas = (Citas) vistafisio.listacitas.getSelectedValue();
        modelo.deleteCita(citas);
            cargardatosFisio();
        }
        break;
        case"modificarcita":{
            Citas citas = (Citas) vistafisio.listacitas.getSelectedValue();
            citas.setPrecio(Double.valueOf(vistafisio.txt_cita_precio.getText()));
            citas.setLugar(vistafisio.txt_fisio_cita_lugar.getText());
            int hora= Integer.parseInt(vistafisio.txt_cita_hora.getText());
            int minutos= Integer.parseInt(vistafisio.txt_cita_minutes.getText());
            Time time=new Time(hora,minutos,0);
            citas.setDuracion(time);
            citas.setFecha(Date.valueOf(vistafisio.fechacita.getDate()));
            modelo.modificarCita(citas);
            limpiarcamposcitas();
            cargardatosFisio();
        }
        break;
        case "deletefisioadmin":{
            boolean puede=comprobarPrivilegiosfisio();
            if (puede) {
                Fisios fisios = (Fisios) vistafisio.listFisios.getSelectedValue();
                String nick = fisios.getNickname();
                Usuarios user = modelo.obtnerusuario(nick);
                Fisios fisios1 = modelo.obtenerfisio(nick);
                Credenciales credenciales = modelo.obtenerCredencial(nick);
                Sesion cesion = new Sesion();
                cesion.setCredenciales(credenciales);
                cesion.setFisios(fisios1);
                cesion.setUsuario(user);
                modelo.deleteAccountFisio(cesion);
                cargardatosFisio();
            }else{
                Util.mensajeWarning("No tienes privilegios ");
            }
        }
        break;
        case"deleteclienteadmin":{
            boolean puede=comprobarPrivilegiosfisio();
            if (puede) {
                vistafisio.listaclients.getSelectedValue();
                Clientes clientes= (Clientes) vistafisio.listaclients.getSelectedValue();
                String nick=clientes.getNickname();
                Usuarios user=modelo.obtnerusuario(nick);
                Credenciales credenciales =modelo.obtenerCredencial(nick);
                modelo.deleteUser(clientes,user, credenciales);
                cargardatosFisio();
            }else{
                Util.mensajeWarning("No tienes privilegios ");
            }
        }
        break;
        case"citasclientes":{
            vistacita=new Vistacita();
            addlistenerClienteCita(this);
            listarcitascliente();
        }
        break;
        case "closecliente": {
            vistacliente.setVisible(false);
            try {
                vistacita.setVisible(false);
                editProfile.setVisible(false);
            }catch (NullPointerException e3){}
           Util.desplegarAplcacion();
        }
        break;
        case "closefisio":{
            login.setVisible(false);
            vistafisio.setVisible(false);
            Util.desplegarAplcacion();
        }
        break;
        case "generarinformes1":{
           modelo.generarInformeCita(cesion.getClientes().getId());
        }break;
        case"generarinforme2":{
            boolean siguiente=modelo.comprobarPrivilegios(cesion.getUsuario().getNickname());
            if (siguiente) {
                modelo.generarInformeUsuarios();
            }else{
                Util.mensajeWarning("No tienes privilegios ");
            }
        }break;
    }


    }

    private void limpiarcamposcitas() {
        vistafisio.txt_cita_hora.setText("");
        vistafisio.txt_cita_minutes.setText("");
        vistafisio.txt_fisio_cita_lugar.setText("");
        vistafisio.txt_cita_precio.setText("");
        vistafisio.fechacita.setText("");
    }

    /**
     * add listener a un campo de texto
     * @param listener {@link KeyListener}
     */
    private void addlistenertxt(KeyListener listener) {
        vistacliente.txt_account_busqueda.addKeyListener(listener);
    }

    /**
     * limpia el defaultlistmodel
     * y recorre un array add objetos
     */
    private void listarcitascliente() {
        vistacita.dlmcitas.clear();
        for (Citas citas :modelo.filtrarCitas(cesion,2)) {
            vistacita.dlmcitas.addElement(citas);
        }
    }

    /**
     * obtiene el id del cliente que hay que modificar
     * @deprecated
     * @param c1 {@link Clientes}
     * @param incidencias {@link Incidencias}
     * @return  cliente modificado
     */
    private Clientes modificacionCiente(Clientes c1, Incidencias incidencias) {
        boolean delete=false;
        Incidencias eliminar=null;
        for (Incidencias i:c1.getDolencias()) {
            if (i.getId()== incidencias.getId()){
                eliminar=i;
            }
        }
        c1.getDolencias().remove(eliminar);
        c1.getDolencias().add(incidencias);
        return c1;
    }

    /**
     * limpiar campos
     * si recibe true limpiara mas campos
     * @param b  boolean
     */
    private void limpiarcampos(boolean b) {
        newaccount.vaciarCampos();
        if (b) {
            campo.txt_estudios.setText("");
        }
    }

    /**
     * add Jlist listener a una lista
     * @param listener {@link ListSelectionListener}
     */
    private void addlistJlistlistener(ListSelectionListener listener) {
        vistafisio.listaincidenciascita.addListSelectionListener(listener);

    }

    /**
     * refresca todos los datos de los fisioterapeutas
     */
    private void refrescarFisios() {
        cargardatosFisio();
    }


    private boolean comprobarPrivilegiosfisio(){
     Boolean permisos= modelo.comprobarPrivilegios(cesion.getFisios().getNickname());
    return permisos;
    }

    private boolean comprobarcamposVaciosFisio() {
        if ((vistafisio.txt_fisio_nombre.getText().isEmpty())||
        (vistafisio.txt_fisio_apellido.getText().isEmpty()||
                (vistafisio.txt_fisio_correo.getText().isEmpty())||
                (vistafisio.txt_fisio_estudios.getText().isEmpty()))){
           return false;
        }else {return true;}

    }




    private void addListenerLabel(ActionListener listener) {

    }

    private Boolean comprobacionReincidente() {
        if (vistacliente.cbx_cliente_reincidente.isSelected()){
            return true;
        }else {
            return false;
        }
    }

    private void listarClientes() {
        vistacliente.dlmIncidencias.clear();
        try {
            modelo.setDolencias(modelo.filtrarDolencia(cesion.getClientes()));

        }catch (NullPointerException e2){

        }
        for (Incidencias d:modelo.getDolencias()) {
                vistacliente.dlmIncidencias.addElement(d);
        }

    }


    private List<Clientes> obtenerPancientes() {
        ArrayList<Clientes>listclientes=new ArrayList<>();
        String user=cesion.getCredenciales().getUser();
        listclientes.add(modelo.addCliente(user));
        return listclientes;
    }

    private Boolean comprobarReincidencia() {
        if (vistacliente.cbx_cliente_reincidente.isSelected()){
            return  true;
        }else {
            return false;
        }
    }

    /**
     * comprueba que que los campos de la dolencia no esten vacios
     * @return boolean
     */
    private boolean comprobarCamposEmptyDolencia() {
        if ((vistacliente.txt_account_busqueda.getText().isEmpty()) &&(vistacliente.txt_cliente_molestia.getText().isEmpty())){
            return false;
        }else {
            return true;
        }
    }

    private void comprobarCamposVacios() {
        if ( editProfile.txt_edit_apellidos.getText().equalsIgnoreCase("")||
                editProfile.txt_edit_name.getText().equalsIgnoreCase("") ||
                editProfile.txt_edit_nick.getText().equalsIgnoreCase("")||
                editProfile.txt_edit_paswd_old.getText().equalsIgnoreCase("")||
                editProfile.txt_edit_paswd_new.getText().equalsIgnoreCase("")){
            Util.mensajeWarning("Te dejaste algún campo vacío");
        }

    }

    private void addlistenerEditProfile(ActionListener listener) {
        editProfile.btn_deshacer_account.addActionListener(listener);
        editProfile.btn_edit_remove_account.addActionListener(listener);
        editProfile.btn_edit_account.addActionListener(listener);

    }
    private void addlistenerClientes(ActionListener listener) {
        vistacliente.btn_account_profile.addActionListener(listener);
        vistacliente.btn_add_dolencia.addActionListener(listener);
        vistacliente.btn_modificar_dolencia.addActionListener(listener);
        vistacliente.btn_delete_dolencia.addActionListener(listener);
        vistacliente.btn_citas_clientes.addActionListener(listener);

    }
    private void addListenercampo(ActionListener listener) {
        campo.btn_fisio_crearac.addActionListener(listener);
        campo.btn_fisio_cancel.addActionListener(listener);

    }
    private void addlistenerfisio(ActionListener listener) {
        vistafisio.btn_fisio_change_passwd.addActionListener(listener);
        vistafisio.btn_fisio_change_modificar.addActionListener(listener);
        vistafisio.btn_fisio_change_deshacer.addActionListener(listener);
        vistafisio.btn_fisio_change_delete.addActionListener(listener);
        vistafisio.btn_fisio_pass.addActionListener(listener);
        vistafisio.btn_fisio_denay.addActionListener(listener);
        vistafisio.btn_cita_add.addActionListener(listener);
        vistafisio.btn_cita_delete.addActionListener(listener);
        vistafisio.btn_cita_modifcar.addActionListener(listener);
        vistafisio.btn_delete_fisios.addActionListener(listener);
        vistafisio.btn_admin_cliente.addActionListener(listener);
        vistafisio.btn_fisio_informe.addActionListener(listener);
    }
    private void addlistnerMenuFisio(ActionListener listener){
        vistafisio.cuenta.addActionListener(listener);
    }
    /**
     * add listener  a los botones
     * @param listener {@link ActionListener}
     */
    private void addlistener(ActionListener listener) {
        login.btnLoginAccoutInit.addActionListener(listener);
        login.btnLoginAccoutRegistrer.addActionListener(listener);
    }

    /**
     * add Listener a un JDialog
     * @param listener {@link ActionListener}
     */
    private void addlistenerregister(ActionListener listener) {
        newaccount.btn_ac_init.addActionListener(listener);
        newaccount.btn_ac_init.setActionCommand("crearus");

    }

    /**
     * add listener a los {@link JButton} de {@link Vistacita}
     * @param listener {@link ActionListener}
     */
    private void addlistenerClienteCita(ActionListener listener) {
        vistacita.btn_cliente_cita.addActionListener(listener);
    }

    /**
     * add listener al menu del cliente
     * @param listener {@link ActionListener}
     */
    private void addlistenermenuCliente(ActionListener listener) {
        vistacliente.cerrar.addActionListener(listener);

    }

    /**
     * add listener al menu del fisio
     * @param listener {@link ActionListener}
     */
    private void addlistenerMenuFisio(ActionListener listener) {
        vistafisio.cerrar.addActionListener(listener);
    }

    /**
     * crea un objeto {@link Clientes} vacio
     * @return objeto {@link Clientes} lleno
     */
    private Clientes crearCliente() {
        Clientes clientes =new Clientes();
        clientes.setNombre(obtenerNombre());
        clientes.setApellidos(obtenerApellido());
        clientes.setDni(obtenerDni());
        clientes.setNickname(obtnerNick());
        clientes.setCorreo(obtenerCorreo());
        clientes.setPasswd(encriptarPasswd(obtenerasswd()));
        clientes.setFechNac(Date.valueOf(newaccount.datePicker.getDate()));
        return clientes;
    }

    /**
     * crear un objeto Fisio vacio
     * @return objeto Fisio relleno
     */
    private Fisios CrearFisio() {
        Fisios fisios =new Fisios();
        fisios.setNombre(obtenerNombre());
        fisios.setApellidos(obtenerApellido());
        fisios.setDni(obtenerDni());
        fisios.setNickname(obtnerNick());
        fisios.setCorreo(obtenerCorreo());
        fisios.setPasswd(encriptarPasswd(obtenerasswd()));

        fisios.setEstudios(campo.txt_estudios.getText());
        fisios.setFechNac(Date.valueOf(newaccount.datePicker.getDate()));
        fisios.setRango((String) campo.cb_fisio.getSelectedItem());
        fisios.setReputacion("regular");
        return fisios;
    }

    /**
     * crea un objeto Credencial vacio
     * @return objeto Credencial lleno
     */
    private Credenciales crearCredencial() {
        Credenciales credenciales =new Credenciales();
        credenciales.setPasswd(encriptarPasswd(obtenerasswd()));
        credenciales.setUser(obtnerNick());
        return credenciales;
    }

    /**
     * crea un objeto Usuario vacio
     * @return objeto Usuario relleno
     */
    private Usuarios crearUsuario() {
        Usuarios users=new Usuarios();
        users.setNombre(obtenerNombre());
        users.setApellidos(obtenerApellido());
        users.setDni(obtenerDni());
        users.setTipo(tipoUsuario());
        users.setNickname(obtnerNick());
        users.setCorreo(obtenerCorreo());
        users.setPasswd(encriptarPasswd(obtenerasswd()));
        users.setFechNac(obtenerFecha());
        return users;
    }

    /**
     * obtiene la fecha de un {@link javafx.scene.control.DatePicker}
     * @return DatePicker
     */
    private Date obtenerFecha() {
        return Date.valueOf(newaccount.datePicker.getDate());
    }

    /**
     * obtienes el texto de un campo
     * @return String texto
     */
    private String obtenerasswd() {
        return  newaccount.txt_ac_passwd.getText();
    }

    /**
     * metodo que encripta  un String
     * @param obtenerasswd cadena a encriptar
     * @return String encriptado
     */
    private String encriptarPasswd(String obtenerasswd) {
        return encriptacion.encriptado("secret", obtenerasswd);
    }
    /**
     * obtienes el texto de un campo
     * @return String texto correo
     */
    private String obtenerCorreo() {
        return  newaccount.txt_ac_correo.getText();
    }
    /**
     * obtienes el texto de un campo
     * @return String texto nick
     */
    private String obtnerNick() {
        return   newaccount.txt_ac_nick.getText();
    }

    /**
     * metodo que devuelve un string dependiendo de item este seleccioando
     * @return String que especifica el tipo de usuario
     */
    private String tipoUsuario() {
        if (newaccount.rb_ac_fisio.isSelected()){
            return "fisio";
        }else{
            return "cliente";
        }
    }

    /**
     * obtiene texto  de un JTextfield
     * @return String dni
     */
    private String obtenerDni() {
        return newaccount.txt_ac_dni.getText();
    }

    /**
     * obtiene texto de un Jtexfield
     * @return String apellido
     */
    private String obtenerApellido() {
        return newaccount.txt_ac_apellido.getText();
    }
    /**
     * obtiene texto de un Jtexfield
     * @return String nombre
     */
    private String obtenerNombre() {
         return newaccount.txt_ac_nombre.getText();
    }

    /**
     * carga los datos de la base de datos asociado al fisioterapeuta
     * filtra y rellena los dlm
     */
    private void cargardatosFisio() {
        vistafisio.dlmlistacliente.clear();
        vistafisio.dlmlistafisio.clear();
        vistafisio.dlmcitas.clear();
        vistafisio.dlmIncidencias.clear();
        vistafisio.dlmIncidenciascita.clear();
        for (Incidencias incidencias :modelo.incidenciaspendientes){
            vistafisio.dlmIncidencias.addElement(incidencias);
        }
        modelo.setIncidenciasDisponibles(modelo.actualizarDolencias());
        for (Incidencias incidencias :modelo.getIncidenciasDisponibles()) {
            if (!incidencias.getEstado().equalsIgnoreCase("pendiente"))
            vistafisio.dlmIncidenciascita.addElement(incidencias);
        }
        modelo.setCitas(modelo.filtrarCitas(cesion,1));
        for (Citas citas :modelo.getCitas()){
            vistafisio.dlmcitas.addElement(citas);
        }
        if (cesion.getFisios().getRango().equalsIgnoreCase("admin")) {
            modelo.setListfisios(modelo.obtenerfisioList());
            for (Fisios f : modelo.getListfisios()) {
                vistafisio.dlmlistafisio.addElement(f);
            }
            modelo.setListclientes(modelo.obtenerClienteList());
            for (Clientes c : modelo.getListclientes()) {
                vistafisio.dlmlistacliente.addElement(c);
            }
        }

    }
    @Override
    public void windowOpened(WindowEvent e) {
        modelo.cargarDatos();
        listarClientes();

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        vistafisio.cb_cita_cliente.removeAllItems();
        Incidencias incidencias = (Incidencias) vistafisio.listaincidenciascita.getSelectedValue();
        List<Clientes>list=modelo.filtraCliente(incidencias);
        ArrayList<String>liststring=new ArrayList<>();
        for (Clientes clientes:list){
            liststring.add(clientes.getNickname());
        }
        for (String s:liststring){
            vistafisio.cb_cita_cliente.addItem(s);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {


    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource()==vistacliente.txt_account_busqueda){
            ArrayList resultado=modelo.filtradoBusquedasIncidencias(vistacliente.txt_account_busqueda.getText());
            filradoIncidenciasClientes(resultado);
            if (vistacliente.txt_account_busqueda.getText().isEmpty()){
                listarClientes();
            }
        }
    }

    /**
     * filtra las incidencias de la busqueda
     * @param resultado {@link ArrayList} de Incidencias
     */
    private void filradoIncidenciasClientes(ArrayList<Incidencias> resultado) {
        vistacliente.dlmIncidencias.clear();
        for (Incidencias is:resultado) {
            vistacliente.dlmIncidencias.addElement( is);
        }
    }
}
