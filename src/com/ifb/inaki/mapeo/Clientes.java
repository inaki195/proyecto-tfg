package com.ifb.inaki.mapeo;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author inaki
 * clase mapeada hace referencia a  los clientes de base de datos
 */
@Entity
public class Clientes {
    private int id;
    private String nombre;
    private String apellidos;
    private String dni;
    private String nickname;
    private String correo;
    private String passwd;
    private Date fechNac;
    private List<Citas> citas;

    private List<Incidencias> dolencias;
    private Usuarios usuario;
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 60)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos", nullable = true, length = 120)
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni", nullable = true, length = 10)
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "nickname", nullable = true, length = 100)
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "correo", nullable = true, length = 120)
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Basic
    @Column(name = "passwd", nullable = true, length = 90)
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "fech_nac", nullable = true)
    public Date getFechNac() {
        return fechNac;
    }

    public void setFechNac(Date fechNac) {
        this.fechNac = fechNac;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clientes clientes = (Clientes) o;
        return id == clientes.id &&
                Objects.equals(nombre, clientes.nombre) &&
                Objects.equals(apellidos, clientes.apellidos) &&
                Objects.equals(dni, clientes.dni) &&
                Objects.equals(nickname, clientes.nickname) &&
                Objects.equals(correo, clientes.correo) &&
                Objects.equals(passwd, clientes.passwd) &&
                Objects.equals(fechNac, clientes.fechNac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dni, nickname, correo, passwd, fechNac);
    }

    @OneToMany(mappedBy = "clientes",cascade = CascadeType.ALL)
    public List<Citas> getCitas() {
        return citas;
    }

    public void setCitas(List<Citas> citas) {
        this.citas = citas;
    }

    @ManyToMany(cascade = CascadeType.ALL ,fetch = FetchType.EAGER)
    @JoinTable(name = "cliente_incidencia", schema = "gestionfisio",
            joinColumns = @JoinColumn(name = "id_cliente", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_incidencia", referencedColumnName = "id"))
    public List<Incidencias> getDolencias() {
        return dolencias;
    }

    @OneToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id",updatable = true)
    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }


    public void setDolencias(List<Incidencias> dolencias) {
        this.dolencias = dolencias;
    }

    @Override
    public String toString() {
        return
                id+" " + nombre  +
                " " + apellidos  +
                " " + dni  +
                " " + nickname ;
    }
}
