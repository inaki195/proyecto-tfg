package com.ifb.inaki.mapeo;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author inaki
 * clase mapeada hace referencia a  las incidencias de base de datos
 */
@Entity
public class Incidencias {
    private int id;
    private String molestia;
    private String nivelDolor;
    private Boolean reincidente;
    private String estado;

    private List<Clientes> clientes;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "molestia", nullable = true, length = 225)
    public String getMolestia() {
        return molestia;
    }

    public void setMolestia(String molestia) {
        this.molestia = molestia;
    }

    @Basic
    @Column(name = "nivel_dolor", nullable = true)
    public String getNivelDolor() {
        return nivelDolor;
    }

    public void setNivelDolor(String nivelDolor) {
        this.nivelDolor = nivelDolor;
    }

    @Basic
    @Column(name = "reincidente", nullable = true)
    public Boolean getReincidente() {
        return reincidente;
    }

    public void setReincidente(Boolean reincidente) {
        this.reincidente = reincidente;
    }

    @Basic
    @Column(name = "estado", nullable = true)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Incidencias that = (Incidencias) o;
        return id == that.id &&
                Objects.equals(molestia, that.molestia) &&
                Objects.equals(nivelDolor, that.nivelDolor) &&
                Objects.equals(reincidente, that.reincidente) &&
                Objects.equals(estado, that.estado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, molestia, nivelDolor, reincidente, estado);
    }
    @ManyToMany( cascade = CascadeType.ALL ,mappedBy = "dolencias")
    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }

    @Override
    public String toString() {
        return id+" " + molestia + " " +
                " " + nivelDolor +
                " " +clientes.toString()+
                " " + estado+" " ;
    }
}
