package com.ifb.inaki.mapeo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

/**
 * @author inaki
 * clases de las cita
 */
@Entity
public class Citas {
    private int id;
    private Double precio;
    private String lugar;
    private Time duracion;
    private Date fecha;
    private Clientes clientes;
    private Fisios fisios;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "precio", nullable = true, precision = 0)
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "lugar", nullable = true, length = 120)
    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Basic
    @Column(name = "duracion", nullable = true)
    public Time getDuracion() {
        return duracion;
    }

    public void setDuracion(Time duracion) {
        this.duracion = duracion;
    }

    @Basic
    @Column(name = "fecha", nullable = true)
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Citas citas = (Citas) o;
        return id == citas.id &&
                Objects.equals(precio, citas.precio) &&
                Objects.equals(lugar, citas.lugar) &&
                Objects.equals(duracion, citas.duracion) &&
                Objects.equals(fecha, citas.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, precio, lugar, duracion, fecha);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    @ManyToOne
    @JoinColumn(name = "id_fisio", referencedColumnName = "id")
    public Fisios getFisios() {
        return fisios;
    }

    public void setFisios(Fisios fisios) {
        this.fisios = fisios;
    }

    @Override
    public String toString() {
        return   id +
                " " + precio +
                "€ " + lugar  +
                " " + duracion +
                " " + fecha ;
    }
}
