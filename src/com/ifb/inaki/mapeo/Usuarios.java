package com.ifb.inaki.mapeo;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;
/**
 * @author inaki
 * clase mapeada hace referencia a  los Usuarios de base de datos
 */
@Entity
public class Usuarios {
    private int id;
    private String nombre;
    private String apellidos;
    private String dni;
    private String tipo;
    private String nickname;
    private String correo;
    private String passwd;
    private Date fechNac;
    private Fisios fisios;
    private Clientes clientes;
    private Credenciales credenciales;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 60)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos", nullable = true, length = 120)
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni", nullable = true, length = 10)
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "tipo", nullable = true)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "nickname", nullable = true, length = 100)
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "correo", nullable = true, length = 120)
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Basic
    @Column(name = "passwd", nullable = true, length = 90)
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "fech_nac", nullable = true)
    public Date getFechNac() {
        return fechNac;
    }

    public void setFechNac(Date fechNac) {
        this.fechNac = fechNac;
    }

    @OneToOne(mappedBy = "usuario")
    public Fisios getFisios() {
        return fisios;
    }

    public void setFisios(Fisios fisios) {
        this.fisios = fisios;
    }

    @OneToOne(mappedBy = "usuario")
    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    @OneToOne(mappedBy = "usuario"  )
    public Credenciales getCredenciales() {
        return credenciales;
    }

    public void setCredenciales(Credenciales credenciales) {
        this.credenciales = credenciales;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuarios usuario = (Usuarios) o;
        return id == usuario.id &&
                Objects.equals(nombre, usuario.nombre) &&
                Objects.equals(apellidos, usuario.apellidos) &&
                Objects.equals(dni, usuario.dni) &&
                Objects.equals(tipo, usuario.tipo) &&
                Objects.equals(nickname, usuario.nickname) &&
                Objects.equals(correo, usuario.correo) &&
                Objects.equals(passwd, usuario.passwd) &&
                Objects.equals(fechNac, usuario.fechNac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dni, tipo, nickname, correo, passwd, fechNac);
    }

    @Override
    public String toString() {
        return "Usuarios{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", tipo='" + tipo + '\'' +
                ", nickname='" + nickname + '\'' +
                ", fisio=" + fisios +
                ", cliente=" + clientes +
                '}';
    }
}
