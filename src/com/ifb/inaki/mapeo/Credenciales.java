package com.ifb.inaki.mapeo;

import javax.persistence.*;
import java.util.Objects;
/**
 * @author inaki
 * clase mapeada hace referencia a  los credenciales de base de datos
 */
@Entity
public class Credenciales {
    private int id;
    private String user;
    private String passwd;
    private Usuarios usuario;


    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user", nullable = true, length = 100)
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Basic
    @Column(name = "passwd", nullable = true, length = 100)
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credenciales that = (Credenciales) o;
        return id == that.id &&
                Objects.equals(user, that.user) &&
                Objects.equals(passwd, that.passwd);
    }
    @OneToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, passwd);
    }
}
