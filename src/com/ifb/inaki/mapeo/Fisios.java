package com.ifb.inaki.mapeo;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
/**
 * @author inaki
 * clase mapeada hace referencia a  los Fisios de base de datos
 */
@Entity
public class Fisios {
    private int id;
    private String estudios;
    private String reputacion;
    private String rango;
    private String nombre;
    private String apellidos;
    private String dni;
    private String nickname;
    private String correo;
    private String passwd;
    private Date fechNac;
    private List<Citas> citas;
    private Usuarios usuario;





    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "estudios", nullable = true, length = 100)
    public String getEstudios() {
        return estudios;
    }

    public void setEstudios(String estudios) {
        this.estudios = estudios;
    }

    @Basic
    @Column(name = "reputacion", nullable = true)
    public String getReputacion() {
        return reputacion;
    }

    public void setReputacion(String reputacion) {
        this.reputacion = reputacion;
    }
   
    @Basic
    @Column(name = "rango", nullable = true)
    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 60)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos", nullable = true, length = 120)
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni", nullable = true, length = 10)
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "nickname", nullable = true, length = 100)
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "correo", nullable = true, length = 120)
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Basic
    @Column(name = "passwd", nullable = true, length = 90)
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "fech_nac", nullable = true)
    public Date getFechNac() {
        return fechNac;
    }

    public void setFechNac(Date fechNac) {
        this.fechNac = fechNac;
    }

    @OneToOne
    @JoinColumn(name = "id_usuario")
    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fisios fisios = (Fisios) o;
        return id == fisios.id &&
                Objects.equals(estudios, fisios.estudios) &&
                Objects.equals(reputacion, fisios.reputacion) &&
                Objects.equals(rango, fisios.rango) &&
                Objects.equals(nombre, fisios.nombre) &&
                Objects.equals(apellidos, fisios.apellidos) &&
                Objects.equals(dni, fisios.dni) &&
                Objects.equals(nickname, fisios.nickname) &&
                Objects.equals(correo, fisios.correo) &&
                Objects.equals(passwd, fisios.passwd) &&
                Objects.equals(fechNac, fisios.fechNac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, estudios, reputacion, rango, nombre, apellidos, dni, nickname, correo, passwd, fechNac);
    }

    @OneToMany(mappedBy = "fisios",cascade=CascadeType.ALL, orphanRemoval = true)
    public List<Citas> getCitas() {
        return citas;
    }

    public void setCitas(List<Citas> citas) {
        this.citas = citas;
    }

    @Override
    public String toString() {
        return " " + id +
                " " + nombre  +
                " " + apellidos +
                " " + nickname +" "
                +reputacion;

    }
}
