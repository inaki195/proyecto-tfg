package com.ifb.inaki.pruebaunit;

import com.ifb.inaki.mapeo.*;
import com.ifb.inaki.mvc.Modelo;
import com.ifb.inaki.util.HibernateUtil;
import encriptado.Encriptacion;
import org.junit.Assert;
import org.junit.Test;


import java.util.ArrayList;

public class TestModelo {
    Modelo modelo;
    Encriptacion encriptacion;
    @Test
    public void rellenarIncidencia(){
        initModel();
        modelo.rellenarIncidenciasGenerales();
        ArrayList<Incidencias>list=modelo.getIncidenciaspendientes();
        boolean relleno=false;
        if (list.size()>0){
            relleno=true;
        }else {
            relleno=false;
        }
        Assert.assertTrue(true);
    }
    @Test
    public void comprobarCredencialess(){
        initModel();
        Credenciales credenciales =new Credenciales();
        credenciales.setPasswd("polo");
        credenciales.setUser("paco");
        boolean esperado=modelo.comprobarCredenciales(credenciales);
        Assert.assertFalse(esperado);
    }
    @Test
    public void comprobarUserTest(){
        initModel();
        String dniInexistente="88866121D";
        Usuarios usuario =modelo.comprobarFisio(dniInexistente);
        Assert.assertNull(usuario);
    }
    @Test
    public void introducirCredencialTest(){
        initModel();
        encriptacion=new Encriptacion();
        ArrayList<Credenciales>listBefore=modelo.obtenerCredencialeslist();
        Credenciales cr=new Credenciales();
        cr.setUser("paco");
        cr.setPasswd(encriptacion.encriptado("secret","set"));
        cr.setUser(null);
        modelo.introducirCredencial(cr);
        ArrayList<Credenciales>listAfter=modelo.obtenerCredencialeslist();
        boolean superior;
        if (listBefore.size()<listAfter.size()){
            superior=true;
        }else {
            superior=false;
        }
        Assert.assertTrue(superior);
    }
    @Test
    public void introducirDolecinaTest(){
        initModel();
        ArrayList<Incidencias>incidenciasbefore=modelo.actualizarDolencias();
        Incidencias incidenciastest=crearIncidencia();
        modelo.introducirDolencia(incidenciastest);
        ArrayList<Incidencias> incidenciasAfter =modelo.actualizarDolencias();
        boolean mayor;
        if (incidenciasbefore.size()< incidenciasAfter.size()){
            mayor=true;
        }else{
            mayor=false;
        }
        Assert.assertTrue(mayor);

    }
    @Test
    public void introducirFisio(){
        initModel();
        ArrayList<Fisios> fisiosBefore =modelo.obtenerfisioList();

        Fisios fisios =crearfisio();
        modelo.introducirFisio(fisios);
        ArrayList<Fisios> fisiosAfter =modelo.obtenerfisioList();

        boolean mayor;
        if (fisiosBefore.size()< fisiosAfter.size()){
            mayor=true;
        }else{
            mayor=false;
        }
        Assert.assertTrue(mayor);
    }
    @Test
    public void introducirCliente(){
        initModel();
        ArrayList<Clientes>clientesBefore=modelo.obtenerClienteList();
        Clientes clientes =creacliente();
        modelo.introducirCliente(clientes);
        ArrayList<Clientes>clientesAfter=modelo.obtenerClienteList();
        boolean mayor;
        if (clientesBefore.size()<clientesAfter.size()){
            mayor=true;
        }else {
            mayor=false;
        }
        Assert.assertTrue(mayor);
    }
    @Test
    public void introducirCita(){
        initModel();
        ArrayList<Citas> citasBefore =modelo.obtenerCitas();
        Citas citas =new Citas();
        citas.setLugar("test");
        modelo.introducirCita(citas);
        ArrayList<Citas> citasAfter =modelo.obtenerCitas();
        boolean mayor;
        if (citasAfter.size()> citasBefore.size()){
            mayor=true;
        }else {
            mayor=false;
        }
        Assert.assertTrue(mayor);
    }
    @Test
    public void introduciruser(){
        initModel();
        ArrayList<Usuarios>userBefore=modelo.obtenerUsuarios();
        Usuarios user=new Usuarios();
        user.setNickname("test");
        System.out.println(userBefore.size());
        modelo.introducirUser(user);
        ArrayList<Usuarios>userAfter=modelo.obtenerUsuarios();
        System.out.println(userAfter.size());
        boolean mayor;
        if (userAfter.size()>userBefore.size()){
            mayor=true;
        }else {
            mayor=false;
        }
        Assert.assertTrue(mayor);
    }
    @Test
    public void obtenerReferenciaUser(){
        initModel();
        Credenciales cr=new Credenciales();
        String passwd=encriptacion.desencriptacion("secret","TMfoe98eGK0=");
        String encriptado=encriptacion.encriptado("secret",passwd);
        cr.setPasswd(encriptado);
        cr.setUser("usuarionotfound");
        Usuarios usr=modelo.obtenerReferenciaUser(cr);
        Assert.assertNull(usr);

    }
    @Test
    public void obtenerCitasTest(){
        initModel();
        ArrayList<Citas>citas=modelo.obtenerCitas();
        boolean contiene;
        if (citas.size()>0){
            contiene=true;
        }else {
            contiene=false;
        }
        Assert.assertTrue(contiene);
    }
    @Test
    public void unicousuarioTest(){
        initModel();
        Usuarios usr=new Usuarios();
        usr.setNickname("usrprueba");
        modelo.introducirUser(usr);
        Assert.assertTrue(modelo.uniqueNickname(usr));

    }
    @Test
    public  void comprobarOldPasswordTest(){
        initModel();
        String des=encriptacion.desencriptacion("secret","ITKnc85qr1x24be5zm940g==");
        boolean existente=modelo.comprobarOldPasswd(des,"mikel18");
        Assert.assertTrue(existente);
    }
    @Test
    public void modificarClienteTest(){
        initModel();
        Clientes c4=creacliente();
        String oldNick=c4.getNickname();
        modelo.introducirCliente(c4);
        c4.setNickname("test2");
        modelo.modificarClienteObjeto(c4);
      Assert.assertNotEquals(modelo.obtenerCliente(oldNick),modelo.obtenerCliente(c4.getNickname()));
    }
    @Test
    public void modificarFsioTest(){
        initModel();
        modelointro();
        Fisios fisios =modelo.obtenerfisio("jemez");
        fisios.setNickname("sebastian");
        modelo.modificarfisioterapeuta(fisios);
        Assert.assertNull(modelo.obtenerfisio("jemez"));

    }
    @Test
    public void modificarCredencialesTest(){
        initModel();
        crearCredencial();
        Credenciales credenciales =modelo.obtenerCredencial("ruben");
        credenciales.setUser("pabloski");
        modelo.modificarCredenciales(credenciales);
        Credenciales cr=modelo.obtenerCredencial("pabloski");
        Assert.assertNotNull(cr);
    }
   @Test
   public void modificarCitaTest(){
        initModel();
        crearcita();
        ArrayList<Citas>list=modelo.obtenerCitas();
        Citas modificar=new Citas();
       for (Citas citas :list) {
           if (citas.getLugar().equalsIgnoreCase("seul")){
               modificar= citas;
           }
       }
       modificar.setLugar("argentina");
       modelo.modificarCita(modificar);
       boolean modificado = false;
       for (Citas c:modelo.obtenerCitas()) {
           if (c.getLugar().equalsIgnoreCase("argentina")){
               modificado=true;
           }
       }
       Assert.assertTrue(modificado);
   }

   @Test
   public void modificarUserTest(){
        initModel();
        crearuser();
        Usuarios modicado = null;
       for (Usuarios u:modelo.obtenerUsuarios()) {
           if (u.getNickname().equalsIgnoreCase("franceso")){
               modicado=u;
           }
       }
       modelo.modificarUserhb(modicado);
   }
   @Test
   public void  deleteUserTest(){
        initModel();
       Usuarios user=new Usuarios();
       user.setNickname("fran");
       modelo.introducirUser(user);
       ArrayList<Usuarios>listusuariosBefore=modelo.obtenerUsuarios();
       modelo.desconectar();
       modelo.conectar();
       modelo.deleteUserElegido(user);
       ArrayList<Usuarios>listusuarioAfter=modelo.obtenerUsuarios();
       boolean menor;
       if (listusuarioAfter.size()<listusuariosBefore.size()){
           menor=true;
       }else {
           menor=false;
       }
       Assert.assertFalse(menor);
   }

   @Test
   public void deleteCredencialTest(){
        initModel();
        Credenciales cr=new Credenciales();
        cr.setUser("omar");
        modelo.introducirCredencial(cr);
        ArrayList<Credenciales>listcrendencialesBefore=modelo.obtenerCredencialeslist();
       System.out.println(listcrendencialesBefore.size());
        modelo.desconectar();
        modelo.conectar();
        modelo.deleteCredencial( modelo.obtenerCredencial("omar"));
        ArrayList<Credenciales>listcrendecialesAfter=modelo.obtenerCredencialeslist();
       System.out.println(listcrendecialesAfter.size());
        boolean menor;
        if (listcrendencialesBefore.size()>listcrendecialesAfter.size()){
            menor=true;
        }else {
            menor=false;
        }
        Assert.assertTrue(menor);
   }
   @Test
   public void deleteClientesTest(){
    initModel();
    Clientes clientes=new Clientes();
    clientes.setNickname("gustabo");
    modelo.introducirCliente(clientes);
    ArrayList<Clientes>listClientes=modelo.obtenerClienteList();
    modelo.desconectar();
    modelo.conectar();
    Clientes clientesdelete = null;
       for (Clientes c1:modelo.obtenerClienteList()) {
           if (c1.getNickname().equalsIgnoreCase("gustabo")){
               clientesdelete=c1;
           }
       }
       modelo.deleteCliente(clientesdelete);
       ArrayList<Clientes>listAfter=modelo.obtenerClienteList();
       boolean menor;
       if (listClientes.size()>listAfter.size()){
           menor=true;
       }else {
           menor=false;
       }
       Assert.assertTrue(menor);
   }
   @Test
   public void deleteFisioTest(){
       initModel();
       Fisios fisios =new Fisios();
       fisios.setNickname("gustabo");
       modelo.introducirFisio(fisios);
       ArrayList<Fisios> listFisiosBefore =modelo.obtenerfisioList();
       modelo.desconectar();
       modelo.conectar();
       Fisios fisiosdelete = null;
       for (Fisios c1:modelo.obtenerfisioList()) {
           if (c1.getNickname().equalsIgnoreCase("gustabo")){
               fisiosdelete=c1;
           }
       }
       modelo.deleteFisio(fisiosdelete);
       ArrayList<Fisios>listAfter=modelo.obtenerfisioList();
       boolean menor;
       if (listFisiosBefore.size()>listAfter.size()){
           menor=true;
       }else {
           menor=false;
       }
       Assert.assertTrue(menor);
   }
   @Test
   public void deleteCitaTest(){
       initModel();
       Citas citas =new Citas();
       citas.setLugar("florida");
       modelo.introducirCita(citas);
       ArrayList<Citas>listBefore=modelo.obtenerCitas();
       modelo.desconectar();
       modelo.conectar();
       Citas eliminar = null;
       for (Citas c:modelo.obtenerCitas()) {
           if (c.getLugar().equalsIgnoreCase("florida")){
               eliminar=c;
           }
       }
       modelo.deleteCita(eliminar);
       ArrayList<Citas>listAfter=modelo.obtenerCitas();
       boolean menor;
       if (listBefore.size()>listAfter.size()){
           menor=true;
       }else{
           menor=false;
       }
       Assert.assertTrue(menor);
   }
   @Test
   public void deleteIncidenciasTest(){
       initModel();
       Incidencias incidencias =new Incidencias();
       incidencias.setMolestia("metarcapio");
       modelo.introducirDolencia(incidencias);
       ArrayList<Incidencias>listBefore=modelo.actualizarDolencias();
       modelo.desconectar();
       modelo.conectar();
       Incidencias eliminar = null;
       for (Incidencias c:modelo.actualizarDolencias()) {
           if (c.getMolestia().equalsIgnoreCase("metarcapio")){
               eliminar=c;
           }
       }
       modelo.deleteIncidencia(eliminar);
       ArrayList<Incidencias>listAfter=modelo.actualizarDolencias();
       boolean menor;
       if (listBefore.size()>listAfter.size()){
           menor=true;
       }else{
           menor=false;
       }
       Assert.assertTrue(menor);
   }


    @Test
    public void comprobarConexion(){
        modelo=new Modelo();
        modelo.conectar();
       Assert.assertTrue(HibernateUtil.ssesionFactory());

    }
    @Test
    public void comprobarDesconexion(){
        modelo=new Modelo();
        modelo.conectar();
        modelo.desconectar();
        Assert.assertFalse(HibernateUtil.ssesionFactory());
    }

    private void modelointro() {
        Fisios fisios =new Fisios();
        fisios.setNickname("jemez");
        modelo.introducirFisio(fisios);
        modelo.desconectar();
        initModel();
    }
    private void crearuser() {
        Usuarios user=new Usuarios();
        user.setNickname("franceso");
        modelo.introducirUser(user);
        modelo.desconectar();
        initModel();
    }
    private void crearcita() {
        Citas citas =new Citas();
        citas.setPrecio(44.00);
        citas.setLugar("seul");
        modelo.desconectar();
        initModel();
    }
    private void crearCredencial() {
        Credenciales credenciales =new Credenciales();
        credenciales.setUser("ruben");
        modelo.introducirCredencial(credenciales);
        modelo.introducirCredencial(credenciales);
        modelo.desconectar();
        initModel();

    }

    private Clientes creacliente() {
        Clientes clientes=new Clientes();
        clientes.setNickname("test");
        return clientes;
    }


    private Fisios crearfisio() {
        Fisios fisios =new Fisios();
        fisios.setNickname("test");
        return fisios;
    }
    private void initModel() {
        modelo = new Modelo();
        modelo.conectar();
        encriptacion=new Encriptacion();
    }

    private Incidencias crearIncidencia() {
        Incidencias incidenciastest=new Incidencias();
        incidenciastest.setNivelDolor("leve");
        incidenciastest.setReincidente(true);
        incidenciastest.setEstado("pendiente");
        incidenciastest.setMolestia("test");
        return incidenciastest;
    }


}
